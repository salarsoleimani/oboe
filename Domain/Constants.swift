//
//  Constants.swift
//  SoundPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Reachability
import CoreTelephony

public enum Constants {
  public enum Keys: String {
    //MARK: - Schedulers name
    case cacheSchedulerQueueName = "com.bekapps.Network.Cache.queue"
    case realmRepository = "com.bekapps.RealmPlatform.Repository"
    //MARK: - Storage Keys
    public enum Authentication: String {
      case refreshToken = "com.bekapps.storagekeys.authentication.token.refresh"
      case accessToken = "com.bekapps.storagekeys.authentication.token.access"
      case UUID = "com.bekapps.storagekeys.authentication.info.uuid"
    }
    public enum User: String {
      case musicCount = "com.bekapps.storagekeys.music.count"
      case latestCreatedPlaylists = "com.bekapps.storagekeys.music.latestTimeForPlaylists"
      case name = ""
    }
    
  }
  public enum DefaultNames: String {
    
    //Main
    case album = "Unknown Album"
    case artist = "Unknown Artist"
    case genre = "Various"
    case title = "Untitled"
    case fileType = "UnknownFile"
    
  }
  public enum EndPoints: String {
    
    //Main
    case defaultBaseUrl = ""//[TODO] write your endpoint here ex: http://example.com/api/v3/
    
    //Login
    case tokenUrl = " "//[TODO] write token route here ex: account/user/token
  }
  
  public enum Info {
    public static let osType = "iOS"
    public static let osVersion = { return UIDevice.current.systemVersion }
    public static let deviceName = { return UIDevice.current.model }
    public static let deviceType = { return UIDevice.current.userInterfaceIdiom == .phone ? "Phone" : "Tablet" }
    public static let appVersion = { return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""}
    public static let connectionType = { () -> String in
      do {
        switch try Reachability().connection {
          
        case .none:
          return ReachabilityConnection.None.rawValue
        case .unavailable:
          return ReachabilityConnection.Unavailable.rawValue
        case .wifi:
          return ReachabilityConnection.Wifi.rawValue

        case .cellular:
          let networkString = CTTelephonyNetworkInfo().currentRadioAccessTechnology
          if networkString == CTRadioAccessTechnologyLTE {
            return ReachabilityConnection.LTE.rawValue
          } else if networkString == CTRadioAccessTechnologyWCDMA {
            return ReachabilityConnection.threeG.rawValue
          } else if networkString == CTRadioAccessTechnologyEdge {
            return ReachabilityConnection.Edge.rawValue
          }
        }
      } catch let err {
        print("error on getting connectinoType with err: \(err)")
        return ""
      }
      return ""
    }
  }
}
