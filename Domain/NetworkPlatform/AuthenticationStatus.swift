//
//  AuthorizationStatus.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

public enum AuthenticationStatus: Int {
  case notDetermined = -1
  case tokenExpired = -2
  case loggedIn = 1
}
