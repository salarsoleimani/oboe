//
//  ReachabilityConnection.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-11-07.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

enum ReachabilityConnection: String {
  case None
  case LTE
  case threeG = "3G"
  case Unavailable
  case Edge
  case Wifi
}
