//
//  ResponseMessage.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
public enum ResponseMessage {
  public struct Base: Codable {
    public let code: Int
    public let message: String
  }
}
