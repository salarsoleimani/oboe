//
//  TokenModel.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

public enum TokenModel: InteractiveModelType {
  public struct Request: Codable {
    public let refreshToken: String
    
    public init(refreshToken: String) {
      self.refreshToken = refreshToken
    }
  }
  
  public struct Response: Codable {
    
    public let token: String
    public let refreshToken: String
    
    public init(AccessToken token: String, refreshToken: String) {
      self.token = token
      self.refreshToken = refreshToken
    }
  }
}
