//
//  NetworkUseCaseProvider.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//


import Foundation

public protocol NetworkUseCaseProvider {
  
  //MARK: - Get Token + Login
  func makeAuthorizationUseCase() -> AuthorizationUseCase
}
