//
//  AudioControllerUseCase.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//


import Foundation
import RxSwift

public protocol AudioControllerUseCase {
  func toArtwork(items: [ArtworkContainedProtocol]) -> Observable<[Artwork]>
  func toMusic(item: Playable) -> Observable<Music>
  func track(music: Music) -> Observable<Void>
  
}
