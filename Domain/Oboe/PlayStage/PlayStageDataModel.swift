//
//  HomePageDataModel.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-08.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

public struct HomePageDataModel: Codable {

  public let albums: [Album]?
  public let forYou: [Music]?
  public let recent: [Music]?
  public let bestOfArtists: [Playlist]?
  public init(albums: [Album]?, forYou: [Music]?, recent: [Music]?, bestOfArtists: [Playlist]?) {
    self.albums = albums
    self.forYou = forYou
    self.recent = recent
    self.bestOfArtists = bestOfArtists
  }
}
