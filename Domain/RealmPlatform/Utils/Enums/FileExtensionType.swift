//
//  FileExtensionType.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-11-04.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

public enum FileExtensionType: String, Codable {
  case mp3
  case wav
  case m4a
  case flac
  case none
}
