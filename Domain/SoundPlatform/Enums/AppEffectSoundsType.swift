//
//  AppEffectSound.swift
//  SoundPlatform
//
//  Created by Salar Soleimani on 2019-09-08.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
public enum AppEffectSoundsType {
  public static let splash = { return Bundle.main.url(forResource: "Splash", withExtension: "mp3")!}
  public static let error = { return Bundle.main.url(forResource: "Error", withExtension: "mp3")!}
}
