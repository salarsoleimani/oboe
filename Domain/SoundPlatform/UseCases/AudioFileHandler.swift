//
//  AudioFileHandler.swift
//  Domain
//
//  Created by Behrad Kazemi on 7/11/19.
//  Copyright © 2019 Behrad Kazemi. All rights reserved.
//

import Foundation
import RxSwift
import MediaPlayer

public protocol AudioFileHandler {
	func handleNewMusic(url: URL) -> Observable<Void>
  func handleNewItunesMusic(url: URL, fileType: FileExtensionType) -> Observable<Void>
  func handleWifiUploader(isStart: Bool, completion: ((String) -> Void)?)
  func handleAppleMusics(_ music: MPMediaItem, url: URL) -> Observable<Void>
}
