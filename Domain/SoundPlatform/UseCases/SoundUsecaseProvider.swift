//
//  SoundUsecaseProvider.swift
//  Domain
//
//  Created by Salar Soleimani on 2019-09-08.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

public protocol SoundUsecaseProvider {
  func makeToolbarUsecase() -> ToolbarUsecase
  func makeFullPlayerUsecase() -> FullPlayerUsecase
  func makeRemoteUsecase() -> RemoteUsecase
  func makeAudioFileHandler() -> AudioFileHandler
}
