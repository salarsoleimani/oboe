//
//  AuthorizationNetwork.swift
//  NetworkPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Domain
import RxSwift

public final class AuthenticationNetwork {
  
  private let network: Network<TokenModel.Response>
  
  init(network: Network<TokenModel.Response>) {
    self.network = network
  }
  
  public func getToken(requestParameter: TokenModel.Request) -> Observable<TokenModel.Response> {
    return network.putItem(Constants.EndPoints.tokenUrl.rawValue, parameters:  requestParameter.dictionary!)
  }
  
}
