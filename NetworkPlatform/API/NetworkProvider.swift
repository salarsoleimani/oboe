//
//  NetworkProvider.swift
//  NetworkPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Domain

final class NetworkProvider {
  private var apiEndpoint: String {
    var endpoint = Constants.EndPoints.defaultBaseUrl.rawValue
    if let baseURL = UserDefaults.standard.string(forKey: Constants.EndPoints.defaultBaseUrl.rawValue), !baseURL.isEmpty {
      endpoint = baseURL
    }
    return endpoint
  }
  
  //MARK: - Login and Authorization
  public func makeAuthorizationNetwork() -> AuthenticationNetwork {
    let network = Network<TokenModel.Response>(apiEndpoint)
    return AuthenticationNetwork(network: network)
  }
}
