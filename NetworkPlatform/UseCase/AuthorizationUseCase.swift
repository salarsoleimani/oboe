//
//  AuthorizationUseCase.swift
//  NetworkPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import RxSwift
import Domain

public final class AuthorizationUseCase: Domain.AuthorizationUseCase {
  
  private let network: AuthenticationNetwork
  
  init(network: AuthenticationNetwork) {
    self.network = network
  }
  
  public func getToken(requestParameter: TokenModel.Request) -> Observable<TokenModel.Response> {
    return network.getToken(requestParameter: requestParameter)
  }
}
