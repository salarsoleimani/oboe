//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.NetworkUseCaseProvider {
  
  private let networkProvider: NetworkProvider
  
  public init() {
    networkProvider = NetworkProvider()
  }
  
  //MARK: - Get Token + Login
  public func makeAuthorizationUseCase() -> Domain.AuthorizationUseCase {
    return AuthorizationUseCase(network: networkProvider.makeAuthorizationNetwork())
  }
}
