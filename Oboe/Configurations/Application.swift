//
//  Application.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Domain
import NetworkPlatform
import SoundPlatform
import RealmPlatform
import SuggestionPlatform
import IQKeyboardManagerSwift

final class Application {
  static let shared = Application()
  
  private let networkUseCaseProvider: NetworkPlatform.UseCaseProvider
  private let soundsUseCaseProvider: SoundPlatform.UseCaseProvider
  private let realmUseCaseProvider: RealmPlatform.UseCaseProvider
  private let suggestionUseCaseProvider: SuggestionPlatform.SuggestionUsecaseProvider
  
  private init() {
    AppAnalytics.setup()
    self.networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    self.realmUseCaseProvider = RealmPlatform.UseCaseProvider()
    let manager = realmUseCaseProvider.makeQueryManager()
    self.soundsUseCaseProvider = SoundPlatform.UseCaseProvider(manager: manager)
    self.suggestionUseCaseProvider = SuggestionPlatform.SuggestionUsecaseProvider(queryManager: realmUseCaseProvider.makeQueryManager())
  }
  
  func configureMainInterface(in window: UIWindow) {
    let mainNavigationController = MainNavigationController()
    window.rootViewController = mainNavigationController
    window.makeKeyAndVisible()
    let splashNavigator = SplashNavigator(navigationController: mainNavigationController, dataBaseServices: realmUseCaseProvider, services: networkUseCaseProvider, soundServices: soundsUseCaseProvider, suggestion: suggestionUseCaseProvider)
    splashNavigator.setup()
  }
  func setupApplicationConfigurations() {
    setAppOpenedCount()
    configureKeyboard()
    createMusicsDirectory()
    checkForNewItunesFileSharingFiles()
  }
  private func configureKeyboard() {
    IQKeyboardManager.shared.enable = true
  }
  private func checkForNewItunesFileSharingFiles() {
    let fileManager = FileManager.default
    if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
      do {
        let fileURLs = try fileManager.contentsOfDirectory(at: tDocumentDirectory, includingPropertiesForKeys: nil)
        fileURLs.forEach { (url) in
          let fileTypeString = url.pathExtension.lowercased()
          soundsUseCaseProvider.makeAudioFileHandler().handleNewItunesMusic(url: url, fileType: FileExtensionType(rawValue: fileTypeString) ?? .none)
        }
        // process files
      } catch let err {
        print("error on getting files inside the documents with err: \(err)")
      }
    }
  }
  private func createMusicsDirectory() {
    let fileManager = FileManager.default
    if let tDocumentDirectory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
      let filePath =  tDocumentDirectory.appendingPathComponent("Musics")
      let artWorksFilePath =  tDocumentDirectory.appendingPathComponent("Artworks")
      
      // Create directory of musics if it doesn't exist
      if !fileManager.fileExists(atPath: filePath.path) {
        do {
          try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let err {
          NSLog("Couldn't create document Musics directory: \(err)")
        }
      }
      // Create directory of atrworks if it doesn't exist
      if !fileManager.fileExists(atPath: artWorksFilePath.path) {
        do {
          try fileManager.createDirectory(atPath: artWorksFilePath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let err {
          NSLog("Couldn't create document Artworks directory: \(err)")
        }
      }
    }
  }
  private func setAppOpenedCount() {
    let key = Constants.Keys.appOpenedCount.rawValue
    if StaticConstants.appOpenedCount > 0 {
      UserDefaults.standard.set(StaticConstants.appOpenedCount + 1, forKey: key)
    }
  }
}
