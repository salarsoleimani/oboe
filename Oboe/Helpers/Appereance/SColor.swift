//
//  SColor.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-13.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import UIKit

enum SColor {
  case darkBlue
  case lightBlue
  case ultraLightBlue
  case lightGray
  case darkGray
  case goldYellow
  case white
  case red
  
  case custom(hexString: String, alpha: Double)
  
  func withAlpha(_ alpha: Double) -> UIColor {
    return self.value.withAlphaComponent(CGFloat(alpha))
  }
}

extension SColor {
  
  var value: UIColor {
    var instanceColor = UIColor.clear
    
    switch self {
    case .custom(let hexValue, let opacity):
      instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
    case .darkBlue:
      instanceColor = UIColor(hexString: "#1C2135")
    case .darkGray:
      instanceColor = UIColor.darkGray
    case .lightBlue:
      instanceColor = UIColor(hexString: "#5A6794")
    case .ultraLightBlue:
      instanceColor = UIColor(hexString: "#8794C0")
    case .lightGray:
      instanceColor = UIColor(hexString: "#E7E9EE")
    case .goldYellow:
      instanceColor = UIColor(hexString: "#D8CDB0")
    case .white:
      instanceColor = UIColor.white
    case .red:
      instanceColor = UIColor(hexString: "#DA2864")
    }
    return instanceColor
  }
}

