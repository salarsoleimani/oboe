//
//  PlaySoundEffect.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-13.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import AudioToolbox

public struct PlaySoundEffect {
  static let shared = PlaySoundEffect()
  
  public func playSound(_ withUrl: URL) {
    var sound: SystemSoundID = 0
    AudioServicesCreateSystemSoundID(withUrl as CFURL, &sound)
    AudioServicesPlaySystemSound(sound)
  }
}
