//
//  CommonAlertVC.swift
//  WeMinder
//
//  Created by Krishna on 21/05/19.
//  Copyright © 2019 Krishna All rights reserved.
//

import UIKit

class CommonAlertVC: UIViewController {
  
  /*
   Usage:
   let actionYes: [String: () -> Void] = [ "    Open setting    " : { (
     Utility.openURL(url: URL(string: UIApplication.openSettingsURLString)?.absoluteString ?? "")
     ) }]
   let actionNo : [String: () -> Void] = [ "Cancel" : { (
     print("tapped NO")
     ) }]
   self.showCustomAlertWith(
     message: "Ooops...!",
     descMsg: "\(StaticConstants.userFullName.count < 1 ? "Hey" :StaticConstants.userFullName), You should give access to oboe to read your musics from apple music library.",
     actions: [actionYes, actionNo])
   */
  @IBOutlet weak var viewContainer: UIView!
  @IBOutlet weak var labelMessage: UILabel!
  @IBOutlet weak var labelDescription: UILabel!
  @IBOutlet weak var buttonCancel: UIButton!
  @IBOutlet weak var buttonOkay: UIButton!
  
  var message: String = ""
  var descriptionMessage: String = ""
  
  var arrayAction: [[String: () -> Void]]?
  var okButtonAct: (() ->())?
 
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    labelDescription.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h5)).instance
    labelMessage.font = SFont(.installed(StaticConstants.fontFamily, .bold), size: .standard(.h5)).instance
    [labelDescription, labelMessage].forEach{$0?.textColor = SColor.goldYellow.value}
    viewContainer.layer.cornerRadius = 20.0
    viewContainer.layer.masksToBounds = true
    viewContainer.backgroundColor = SColor.lightBlue.value
    
    buttonOkay.backgroundColor = SColor.goldYellow.value
    buttonOkay.layer.cornerRadius = 10
    
    self.labelMessage.text = message
    self.labelDescription.text = descriptionMessage
    
    if arrayAction == nil {
      buttonCancel.isHidden = true
    } else {
      var count = 0
      for dic in arrayAction! {
        if count > 1 {
          return
        }
        let allKeys = Array(dic.keys)
        let buttonTitle: String = allKeys[0]
        if count == 0 {
          buttonOkay.setAttributedTitle(buttonTitle.toAttributedStringConfirmButton(), for: .normal)
        } else {
          buttonCancel.setAttributedTitle(buttonTitle.toAttributedStringCancelButton(), for: .normal)
        }
        count += 1
      }
    }
  }
  
  // MARK: - IBAction Methods

  
  @IBAction func cancelButtonAction(sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
    if arrayAction != nil {
      let dic = arrayAction![1]
      for (_, value) in dic {
        let action: () -> Void = value
        action()
      }
    } else {
      okButtonAct?()
    }
  }
  
  @IBAction func okayButtonAction(sender: UIButton) {
    self.dismiss(animated: true, completion: nil)
    if arrayAction != nil {
      let dic = arrayAction![0]
      for (_, value) in dic {
        let action: () -> Void = value
        action()
      }
    } else {
      okButtonAct?()
    }
  }
  static func showAlertWithTitle(_ title: String?, message : String?, actionDic : [String: (UIAlertAction) -> Void]) {
    var alertTitle : String = title!
    if title == nil {
      alertTitle = ""
    }
    let alert : UIAlertController = UIAlertController.init(title: alertTitle, message: message, preferredStyle: .alert)
    
    for (key, value) in actionDic {
      let buttonTitle : String = key
      let action: (UIAlertAction) -> Void = value
      alert.addAction(UIAlertAction.init(title: buttonTitle, style: .default, handler: action))
    }
    UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true, completion: nil)
  }
  
}
