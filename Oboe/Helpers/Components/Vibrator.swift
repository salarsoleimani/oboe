//
//  Vibrator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-13.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

struct Vibrator {
  static func vibrate(hardness: Int) {
    if #available(iOS 10.0, *) {
      switch hardness {
      case 1:
        let generator = UINotificationFeedbackGenerator()
        generator.prepare()

        generator.notificationOccurred(.error)
        
      case 2:
        let generator = UINotificationFeedbackGenerator()
        generator.prepare()

        generator.notificationOccurred(.success)
        
      case 3:
        let generator = UINotificationFeedbackGenerator()
        generator.prepare()

        generator.notificationOccurred(.warning)
        
      case 4:
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.prepare()
        generator.impactOccurred()
      case 5:
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.prepare()

        generator.impactOccurred()
        
      case 6:
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.prepare()

        generator.impactOccurred()
        
      default:
        let generator = UISelectionFeedbackGenerator()
        generator.prepare()
          
        generator.selectionChanged()
      }
    }
  }
}
