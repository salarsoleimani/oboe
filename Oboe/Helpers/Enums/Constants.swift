//
//  Constants.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-13.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

enum Constants {
  enum Keys: String {
    case fontScale = "com.storageKey.fontScale"
    case fontFamily = "com.storageKey.fontFamily"
    
    case appOpenedCount = "com.storageKey.appOpenedCount"
    case userFullName = "com.storageKey.userFullName"
  }
  enum Cell: String {
    case cellId
  }
  enum Strings: String {
    // Shared
    case oboe = "oboe"
    case placeHolder = "Type here"
    case hey = "Hey"
    case on = "On"
    case off = "Off"

    // SetNameController
    case nameForFirstTime = "It seems you are using oboe for the first time? 😎"
    case didntEnterNameYet = """
    Hi 👋🏽,
    You didn't enter your name yet
    """
    case whatIsYourName = "What's your name?"
    case changeItLater = "You can change it later in setting"
    case myNameIs = "My name is"

    // ImportMusicsController
    case importDescription = """
    , You didn't import any music yet. Do you want to add some?
    """
    case openBrowserPlease = "Open your browser and visit this link:"
    case wifiSwitch = "Wifi is: "
    case importFrom = "Import from"
    
    // HomeController
    case homeTitle = "Home"
    
    // SettingController
    case settingTitle = "Setting"
  }
  public enum ButtonsText: String {
    case save = "    Save    "
    case dontTellMyName = "I don't wanna tell my name"
    
    case wifi = "    Wifi    "
    case musicLibrary = "    Music Library    "
    case itunes = "    Itunes    "
    case importLater = "I'll import later"

  }
  public enum ErrorStrings: String {
    case enterCorrectName = "Please enter right name"
  }
  public enum SoundEffect {
    public static let splash = { return Bundle.main.url(forResource: "Splash", withExtension: "mp3")!}
    public static let error = { return Bundle.main.url(forResource: "Error", withExtension: "mp3")!}
  }
}
