//
//  StaticConstants.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-13.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

struct StaticConstants {
  static let fontFamily: SFont.FontFamily = UserDefaults.standard.object(forKey: Constants.Keys.fontFamily.rawValue) as? SFont.FontFamily ?? SFont.FontFamily.montserrat
  
  static let appOpenedCount: Int = UserDefaults.standard.integer(forKey: Constants.Keys.appOpenedCount.rawValue)
  static let userFullName: String = UserDefaults.standard.string(forKey: Constants.Keys.userFullName.rawValue) ?? ""
  
  static let mainScreenWidth = UIScreen.main.bounds.width
  static let mainScreenHeight = UIScreen.main.bounds.height
}
