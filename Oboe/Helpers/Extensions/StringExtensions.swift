//
//  StringExtensions.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-24.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation

extension String {
  func toAttributedStringConfirmButton() -> NSAttributedString {
    return NSAttributedString(string: self, attributes: [NSAttributedString.Key.font : SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h4)).instance,
                                                         NSAttributedString.Key.foregroundColor: SColor.white.value
      ])
  }
  func toAttributedStringCancelButton() -> NSAttributedString {
    return NSAttributedString(string: self, attributes: [NSAttributedString.Key.font : SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h6)).instance,
                                                         NSAttributedString.Key.foregroundColor: SColor.ultraLightBlue.value
      ])
  }
}
