//
//  MainNavigationController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-08.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
  // MARK: - Initialization
  init() {
    super.init(rootViewController: UIViewController())
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  override init(rootViewController: UIViewController) {
    super.init(rootViewController: rootViewController)
  }
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setNavigationBarHidden(true, animated: false)
  }
}

