//
//  PageMenuController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-10.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Parchment

class PageMenuController: UIViewController {
  var pageMenuVC: FixedPagingViewController?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  func setupPageMenu(homeVC: HomeController, settingVC: SettingController) {
    homeVC.title = Constants.Strings.homeTitle.rawValue
    settingVC.title = Constants.Strings.settingTitle.rawValue
    pageMenuVC = FixedPagingViewController(viewControllers: [
      homeVC,
      settingVC
    ])
    addChild(pageMenuVC!)
    view.addSubview(pageMenuVC!.view)
    view.constrainToEdges(pageMenuVC!.view)
    pageMenuVC!.didMove(toParent: self)
    setupPageMenuAppereance()
//    let parameters: [CAPSPageMenuOption] = [
//      .scrollMenuBackgroundColor(SColor.darkBlue.value) ,
//      .viewBackgroundColor(SColor.darkBlue.value),
//      .selectionIndicatorColor(SColor.goldYellow.value),
//      .unselectedMenuItemLabelColor(SColor.darkGray.value),
//      .bottomMenuHairlineColor(UIColor.clear),
//      .menuItemFont(SFont.init(.installed(SFont.FontFamily.montserrat, SFont.FontStyle.bold), size: SFont.FontSize.standard(.h1)).instance),
//      .menuHeight(50),
//      .menuMargin(20),
//      .selectionIndicatorHeight(4.0),
//      .menuItemWidthBasedOnTitleTextWidth(true),
//      .selectedMenuItemLabelColor(SColor.goldYellow.value)
//    ]
//
    // Initialize scroll menu
//
//    pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height), pageMenuOptions: parameters)
//
//    self.addChild(pageMenu!)
//    view.addSubview(pageMenu!.view)
//    self.pageMenu?.didMove(toParent: self)
//    pageMenu?.moveToPage(0)
//
  }
  private func setupPageMenuAppereance() {
    pageMenuVC!.menuItemSize = .sizeToFit(minWidth: 100, height: 50)
    //pageMenuVC!.indicatorColor = SColor.goldYellow.value
    pageMenuVC!.backgroundColor = SColor.darkBlue.value
    pageMenuVC!.menuBackgroundColor = SColor.darkBlue.value
    pageMenuVC!.textColor = SColor.darkGray.value
    pageMenuVC!.selectedTextColor = SColor.goldYellow.value
    pageMenuVC!.selectedBackgroundColor = SColor.darkBlue.value
    pageMenuVC!.font = SFont.init(.installed(SFont.FontFamily.montserrat, SFont.FontStyle.regular), size: SFont.FontSize.standard(.h3)).instance
    pageMenuVC!.selectedFont = SFont.init(.installed(SFont.FontFamily.montserrat, SFont.FontStyle.bold), size: SFont.FontSize.standard(.h1)).instance
  }
}
