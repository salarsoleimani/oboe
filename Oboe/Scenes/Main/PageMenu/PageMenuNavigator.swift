//
//  PageMenuNavigator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-10.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

class PageMenuNavigator {
  private let pageMenuVC: PageMenuController
  private let navigationController: UINavigationController
  private let services: Domain.NetworkUseCaseProvider
  private let soundServices: Domain.SoundUsecaseProvider
  private let dataBaseUsecase: Domain.DataBaseUsecaseProvider
  private let suggestion: Domain.SuggestionUsecase
  
  init(services: Domain.NetworkUseCaseProvider, dataBaseUsecase: Domain.DataBaseUsecaseProvider, soundServices: Domain.SoundUsecaseProvider, navigationController: UINavigationController, suggestion: Domain.SuggestionUsecase, pageMenuVC: PageMenuController) {
    self.navigationController = navigationController
    self.services = services
    self.soundServices = soundServices
    self.dataBaseUsecase = dataBaseUsecase
    self.suggestion = suggestion
    self.pageMenuVC = pageMenuVC
  }
  
  func setup(withIndex index: Int = 0) {
    navigationController.modalTransitionStyle = .crossDissolve
    navigationController.pushViewController(pageMenuVC, animated: true)
  
    //let playStageNavigator = PlayStageNavigator(services: services, soundServices: soundServices, navigationController: navigationController, dataBaseUsecase: dataBaseUsecase)
    //let playStageViewModel = PlayStageViewModel(navigator: playStageNavigator, playerUsecase: soundServices.makeToolbarUsecase(), dataUsecase: dataBaseUsecase.makePlayStageUseCase(suggestion: suggestion, fileHandler: soundServices.makeAudioFileHandler()))
    //let playStageViewController = PlayStageViewController(nibName: "PlayStageViewController", bundle: nil)
    
    //playStageViewController.viewModel = playStageViewModel
    let settingNavigator = SettingNavigator(navigationController: navigationController, dataBaseServices: dataBaseUsecase, services: services, soundServices: soundServices, suggestion: suggestion)
    let settingVC = SettingController(navigator: settingNavigator)
    
    let homeNavigator = HomeNavigator(navigationController: navigationController, dataBaseServices: dataBaseUsecase, services: services, soundServices: soundServices, suggestion: suggestion)
    
    let homeVC = HomeController(navigator: homeNavigator, homePageUseCase: dataBaseUsecase.makeHomePageUseCase(suggestion: suggestion))
    
    pageMenuVC.setupPageMenu(homeVC: homeVC, settingVC: settingVC)
  }
  
  func toIndex(index: Int) {
    pageMenuVC.pageMenuVC?.select(index: index, animated: true)
  }
}
