//
//  MusicHorizontalListCell.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-11-11.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Domain

class MusicHorizontalListCell: UICollectionViewCell {
  
  @IBOutlet weak var artworkCover: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var albumLabel: UILabel!
  @IBOutlet weak var indexLabel: UILabel!
  
  @IBOutlet weak var favoriteButton: UIButton!
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  func configure(_ with: Music, index: Int) {
    titleLabel.text = with.title
    albumLabel.text = with.albumName
    indexLabel.text = String(index)
    switch with.liked {
    case true:
      favoriteButton.setImage(#imageLiteral(resourceName: "ic_favorite"), for: .normal)
    case false:
      favoriteButton.setImage(#imageLiteral(resourceName: "ic_unFavorite"), for: .normal)
    }
  }
  
  @IBAction func favoriteButtonPressed(_ sender: Any) {
  }
  @IBAction func moreButtonPressed(_ sender: Any) {
  }
}
