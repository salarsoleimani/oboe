//
//  HomeController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-11-04.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

class HomeController: UIViewController {
  @IBOutlet weak var forYouCollectionView: UICollectionView!
  // MARK:- Constants
  private let navigator: HomeNavigator
  private let homePageUseCase: Domain.HomePageUseCase
  private let disposeBag = DisposeBag()
  // MARK:- Initialization
  init(navigator: HomeNavigator, homePageUseCase: Domain.HomePageUseCase) {
    self.navigator = navigator
    self.homePageUseCase = homePageUseCase
    super.init(nibName: "HomeController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  // MARK:- LifeCycles
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    let errorTracker = ErrorTracker()
    let fetchingTracker = ActivityIndicator()
    let dataResponse = homePageUseCase.getDataModel().trackError(errorTracker).trackActivity(fetchingTracker)
    let forYouSection = dataResponse.flatMapLatest { (response) -> Observable<[Music]> in
      return Observable.just(response.forYou ?? [Music]())
    }
    
    forYouSection
      .asDriverOnErrorJustComplete()
      .drive(forYouCollectionView.rx.items(cellIdentifier: Constants.Cell.cellId.rawValue, cellType: MusicHorizontalListCell.self)) { item, viewModel, cell in
      cell.configure(viewModel, index: item + 1)
    }.disposed(by: disposeBag)
  }
  
  // MARK:- Fuctions
  func setupUI() {
    view.backgroundColor = SColor.red.value
    
    navigationController?.setNavigationBarHidden(false, animated: false)
    if #available(iOS 11.0, *) {
      navigationController?.navigationBar.prefersLargeTitles = true
    }
  }
}
