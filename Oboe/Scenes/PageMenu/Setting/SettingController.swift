//
//  SettingController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-10-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

class SettingController: UIViewController {
  // MARK:- Constants
  private let navigator: SettingNavigator
  
  // MARK:- Initialization
  init(navigator: SettingNavigator) {
    self.navigator = navigator
    super.init(nibName: "SettingController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK:- LifeCycles
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  // MARK:- Fuctions
  func setupUI() {
    view.backgroundColor = SColor.darkBlue.value
    navigationItem.title = "Setting"

    navigationController?.setNavigationBarHidden(false, animated: false)
    if #available(iOS 11.0, *) {
      navigationController?.navigationBar.prefersLargeTitles = true
    }
  }
}
