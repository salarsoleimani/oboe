//
//  SettingNavigator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-10-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

class SettingNavigator {
  private let navigationController: UINavigationController
  private let services: NetworkUseCaseProvider
  private let soundServices: SoundUsecaseProvider
  private let dataServices: DataBaseUsecaseProvider
  private let suggestion: SuggestionUsecase
  
  init(navigationController: UINavigationController, dataBaseServices: DataBaseUsecaseProvider, services: NetworkUseCaseProvider, soundServices: SoundUsecaseProvider, suggestion: SuggestionUsecase) {
    self.navigationController = navigationController
    self.services = services
    self.soundServices = soundServices
    self.dataServices = dataBaseServices
    self.suggestion = suggestion
  }
}
