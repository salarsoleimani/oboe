//
//  ImportMusicsController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-23.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import MediaPlayer

class ImportMusicsController: UIViewController {
  
  // MARK:- Outlets
  @IBOutlet weak var linkUrlView: UIView!
  @IBOutlet weak var linkUrlLabel: UILabel!
  @IBOutlet weak var openBrowserLabel: UILabel!
  
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var logoLabel: UILabel!
  
  @IBOutlet weak var descriptionLabel: UILabel!
  
  @IBOutlet weak var importFromLabel: UILabel!
  
  @IBOutlet weak var musicLibraryButton: UIButtonX!
  @IBOutlet weak var wifiButton: UIButtonX!
  @IBOutlet weak var wifiStatusLabel: UILabel!
  @IBOutlet weak var itunesButton: UIButtonX!
  
  @IBOutlet weak var iWillImportButton: UIButton!
  
  // MARK:- Constants
  private let navigator: ImportMusicsNavigator
  private let fileHandler: AudioFileHandler
  private let disposeBag = DisposeBag()
  
  // MARK:- Variables
  var wifiSwitch = false
  var delegate: SplashControllerDelegate?

  // MARK:- Initialization
  init(navigator: ImportMusicsNavigator, fileHandler: AudioFileHandler) {
    self.navigator = navigator
    self.fileHandler = fileHandler
    super.init(nibName: "ImportMusicsController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK:- LifeCycles
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    makeAnimations()
  }
  // MARK:- Fuctions
  func mediaPicker() -> MPMediaPickerController {
    let mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
    mediaPicker.allowsPickingMultipleItems = true
    mediaPicker.prompt = "Select songs to import in oboe"
    mediaPicker.delegate = self
    return mediaPicker
  }
  
  private func openMusicLibraryAlert() {
    checkMusicAuthorization()
  }
  private func wifiButtonPressed() {
    wifiSwitch = !wifiSwitch
    wifiStatusLabel.text = Constants.Strings.wifiSwitch.rawValue + (wifiSwitch ? Constants.Strings.on.rawValue : Constants.Strings.off.rawValue)
    linkUrlView.moveY(wifiSwitch ? 70 : -70).duration(0.2).easing(.linear).animate()
    logoLabel.moveY(wifiSwitch ? 44 : -44).duration(0.2).easing(.linear).animate()
    logoImageView.moveY(wifiSwitch ? 44 : -44).duration(0.2).easing(.linear).animate()
    
    fileHandler.handleWifiUploader(isStart: wifiSwitch) { [weak self] (url) in
      self?.linkUrlLabel.text = url
    }
  }
  private func itunesButtonPressed() {
    
  }
  private func importLaterButtonPressed() {
    dismiss(animated: true, completion: nil)
    delegate?.addMusicsFinished()
  }
  
  func handleMusics(isAllMusic: Bool, items: [MPMediaItem]?) -> Observable<Void> {
    let musicItems = isAllMusic ? MPMediaQuery.songs().items : items
    if let itunesSongs = musicItems?.compactMap({ (item) -> Observable<Void>? in
      if item.mediaType == .music || item.mediaType == .anyAudio || item.mediaType == .audioITunesU, !item.isCloudItem, !item.hasProtectedAsset, let url = item.assetURL {
        return fileHandler.handleAppleMusics(item, url: url).take(1).share(replay: 1, scope: .forever)
      }
      return nil
    }) {
      return Observable.concat(itunesSongs)
    }
    
    return Observable.just(())
  }
  private func openAlertForRequestMusicAuthorization() {
    DispatchQueue.main.async {
      self.navigator.openAlertForRequestMusicAuthorization { [unowned self] (alert) in
        self.present(alert, animated: true)
      }
    }
  }
  private func openAlertForImportMusic() {
    navigator.openAlertForImportMusic { [unowned self] (alert, action) in
      if action == nil {
        DispatchQueue.main.async {
          self.present(alert, animated: true)
        }
      } else {
        if action?.title == "Select" {
          self.present(self.mediaPicker(), animated: true)
        } else {
          self.handleMusics(isAllMusic: true, items: nil).subscribe().disposed(by: self.disposeBag)
        }
      }
    }
  }
  public func checkMusicAuthorization() {
    let status = MPMediaLibrary.authorizationStatus()
    if status == .authorized {
      openAlertForImportMusic()
    } else if status == .denied || status == .restricted {
      openAlertForRequestMusicAuthorization()
    } else {
      MPMediaLibrary.requestAuthorization { [unowned self] (status) in
        if status == .denied || status == .restricted {
          self.openAlertForRequestMusicAuthorization()
        } else if status == .authorized {
          self.openAlertForImportMusic()
        }
      }
    }
  }
  
  // MARK:- Actions
  @IBAction private func buttonsClicked(_ sender: UIButton) {
    switch sender.tag {
    case 0: // MusicLibrary Button
      openMusicLibraryAlert()
    case 1: // Wifi Button
      wifiButtonPressed()
    case 2: // Itunes Button
      itunesButtonPressed()
    case 3: // I'll import later Button
      importLaterButtonPressed()
    default:
      print("Another button tags pressed which is not in this page.")
    }
  }
}
extension ImportMusicsController: MPMediaPickerControllerDelegate {
  func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems
    mediaItemCollection: MPMediaItemCollection) {
    mediaPicker.dismiss(animated: true, completion: nil)
    handleMusics(isAllMusic: false, items: mediaItemCollection.items).subscribe().disposed(by: self.disposeBag)
  }
  func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
    mediaPicker.dismiss(animated: true, completion: nil)
  }
}
