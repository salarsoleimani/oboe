//
//  ImportMusicsNavigator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-23.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

class ImportMusicsNavigator {
  private let navigationController: UINavigationController
  private let services: NetworkUseCaseProvider
  private let soundServices: SoundUsecaseProvider
  private let dataServices: DataBaseUsecaseProvider
  private let suggestion: SuggestionUsecase
  
  init(navigationController: UINavigationController, dataBaseServices: DataBaseUsecaseProvider, services: NetworkUseCaseProvider, soundServices: SoundUsecaseProvider, suggestion: SuggestionUsecase) {
    self.navigationController = navigationController
    self.services = services
    self.soundServices = soundServices
    self.dataServices = dataBaseServices
    self.suggestion = suggestion
  }
  func openAlertForRequestMusicAuthorization(completion: @escaping (UIAlertController)->()) {
    let alert = UIAlertController(title: "Ooops...!", message: "\(StaticConstants.userFullName.count < 1 ? "Hey" :StaticConstants.userFullName), You should give access to oboe to read your musics from apple music library.", preferredStyle: .alert)
    let openSettingAction = UIAlertAction(title: "Open setting", style: .default, handler: { _ in
      Utility.openURL(url: URL(string: UIApplication.openSettingsURLString)?.absoluteString ?? "")
    })
    alert.view.tintColor = SColor.lightBlue.value // change text color of the buttons
    alert.addAction(openSettingAction)
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    completion(alert)
  }
  func openAlertForImportMusic(completion: @escaping (UIAlertController, UIAlertAction?)->()) {
    let alert = UIAlertController(title: "Import ways", message: "\(StaticConstants.userFullName.count < 1 ? "Hey" :StaticConstants.userFullName), You can import all the musics or select them in your library.", preferredStyle: .actionSheet)
    let musicPickerAction = UIAlertAction(title: "Select", style: .default, handler: { action in
      completion(alert, action)
    })
    let importAllAction = UIAlertAction(title: "Import All", style: .default, handler: { action in
      completion(alert, action)
    })
    alert.view.tintColor = SColor.lightBlue.value // change text color of the buttons
    alert.addAction(musicPickerAction)
    alert.addAction(importAllAction)
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    completion(alert, nil)
  }
}
