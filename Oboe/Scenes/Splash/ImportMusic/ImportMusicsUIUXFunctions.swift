//
//  ImportMusicsUIUXFunctions.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-10-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

extension ImportMusicsController {
  func setupUI() {
    view.backgroundColor = SColor.darkBlue.value
    
    linkUrlLabel.text = ""
    linkUrlLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h6)).instance
    
    openBrowserLabel.text = Constants.Strings.openBrowserPlease.rawValue
    openBrowserLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h6)).instance
    
    logoLabel.text = Constants.Strings.oboe.rawValue
    logoLabel.font = SFont(.custom(SFont.FontFamily.aristotelicaDisplayRegular.rawValue), size: .custom(24)).instance
    
    descriptionLabel.text = (StaticConstants.userFullName.count < 1 ? Constants.Strings.hey.rawValue : StaticConstants.userFullName) + Constants.Strings.importDescription.rawValue
    descriptionLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h5)).instance
    descriptionLabel.alpha = 0
    
    importFromLabel.text = Constants.Strings.importFrom.rawValue
    importFromLabel.font = SFont(.installed(StaticConstants.fontFamily, .bold), size: .standard(.h5)).instance
    importFromLabel.alpha = 0
    
    wifiStatusLabel.text = Constants.Strings.wifiSwitch.rawValue + (wifiSwitch ? Constants.Strings.on.rawValue : Constants.Strings.off.rawValue)
    wifiStatusLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h7)).instance
    wifiStatusLabel.alpha = 0
    
    [importFromLabel, descriptionLabel, logoLabel, wifiStatusLabel].forEach{$0?.textColor = SColor.goldYellow.value}
    [linkUrlLabel, openBrowserLabel].forEach{$0?.textColor = SColor.white.value}
    wifiButton.setAttributedTitle(Constants.ButtonsText.wifi.rawValue.toAttributedStringConfirmButton(), for: .normal)
    musicLibraryButton.setAttributedTitle(Constants.ButtonsText.musicLibrary.rawValue.toAttributedStringConfirmButton(), for: .normal)
    
    itunesButton.setAttributedTitle(Constants.ButtonsText.itunes.rawValue.toAttributedStringConfirmButton(), for: .normal)
    
    [wifiButton, itunesButton, musicLibraryButton].forEach{$0?.backgroundColor = SColor.lightBlue.value}
    
    iWillImportButton.setAttributedTitle(Constants.ButtonsText.importLater.rawValue.toAttributedStringCancelButton(), for: .normal)
    
  }
  func makeAnimations(_ completion: (() -> Void)? = nil) {
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
      self.descriptionLabel.alpha = 1
    })
    UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
      self.importFromLabel.alpha = 1
    })
    UIView.animate(withDuration: 0.25, delay: 1, options: .curveEaseIn, animations: {
      self.wifiStatusLabel.alpha = 1
      completion?()
    })
  }
}
