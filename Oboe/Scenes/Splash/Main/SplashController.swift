//
//  SplashController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Stellar

protocol SplashControllerDelegate: class {
  func setNameFinished(haveMusic: Bool)
  func addMusicsFinished()
}
class SplashController: UIViewController {
  @IBOutlet weak var logoLabel: UILabel!
  @IBOutlet weak var logoImageView: UIImageView!

  var viewModel: SplashViewModel!

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupUI()
    makeAnimations()
  }
//  override func viewDidLoad() {
//    super.viewDidLoad()
//    setupUI()
//  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    makeAnimations {
      if StaticConstants.appOpenedCount < 1 {
        Utility.delay(2, closure: {
          self.viewModel.popSetNameIfNeeded(isFirstTime: true, delegate: self)
        })
      } else if StaticConstants.userFullName.count < 1, StaticConstants.appOpenedCount % 3 == 0 {
        Utility.delay(2, closure: {
          self.viewModel.popSetNameIfNeeded(isFirstTime: false, delegate: self)
        })
      } else {
        self.viewModel.musicsCount(completion: { (musicsCount) in
          if musicsCount < 1 {
            self.viewModel.popAddMusicsIfNeeded(delegate: self)
          }
        })
      }
    }
  }
  
  private func setupUI() {
    view.backgroundColor = SColor.darkBlue.value
    
    logoLabel.text = Constants.Strings.oboe.rawValue
    logoLabel.alpha = 0
    logoLabel.font = SFont(.custom(SFont.FontFamily.aristotelicaDisplayRegular.rawValue), size: .custom(44)).instance
    logoLabel.textColor = SColor.goldYellow.value
  }
  private func makeAnimations(_ completion: (() -> Void)? = nil) {
    logoImageView.moveY(-52).duration(0.3).easing(.linear).animate()
    
    UIView.animate(withDuration: 0.25, delay: 0.2, options: .curveEaseIn, animations: {
      self.logoLabel.alpha = 1
      PlaySoundEffect.shared.playSound(Constants.SoundEffect.splash())
      Vibrator.vibrate(hardness: 6)
      completion?()
    })
  }
}

extension SplashController: SplashControllerDelegate {
  func setNameFinished(haveMusic: Bool) {
    if !haveMusic {
      Utility.delay(1) {
        self.viewModel.popAddMusicsIfNeeded(delegate: self)
      }
    } else {
      viewModel.showPageMenu()
    }
  }
  func addMusicsFinished() {
    viewModel.showPageMenu()
  }
}

