//
//  SplashNavigator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

class SplashNavigator {
  private let navigationController: UINavigationController
  private let services: NetworkUseCaseProvider
  private let soundServices: SoundUsecaseProvider
  private let dataServices: DataBaseUsecaseProvider
  private let suggestion: SuggestionUsecase
  
  init(navigationController: UINavigationController,dataBaseServices: DataBaseUsecaseProvider, services: NetworkUseCaseProvider, soundServices: SoundUsecaseProvider, suggestion: SuggestionUsecase) {
    self.navigationController = navigationController
    self.services = services
    self.soundServices = soundServices
    self.dataServices = dataBaseServices
    self.suggestion = suggestion
  }
  
  func setup() {
    let splashVC = SplashController(nibName: "SplashController", bundle: nil)
    splashVC.viewModel = SplashViewModel(navigator: self, queryManager: dataServices.makeQueryManager())
    navigationController.viewControllers = [splashVC]
  }
  
  func toPageMenu() {
    let pageMenuNavigator = PageMenuNavigator(services: services, dataBaseUsecase: dataServices, soundServices: soundServices, navigationController: navigationController, suggestion: suggestion, pageMenuVC: PageMenuController())
    pageMenuNavigator.setup()
  }
  func toSetName(isFirstTime: Bool, delegate: SplashController) {
    let navigator = SetNameNavigator(navigationController: navigationController, dataBaseServices: dataServices, services: services, soundServices: soundServices, suggestion: suggestion)
    let setNameVC = SetNameController(navigator: navigator, queryManager: dataServices.makeQueryManager())
    setNameVC.delegate = delegate
    setNameVC.isFirstTime = isFirstTime
    setNameVC.loadView()
    setNameVC.setupUI()
    if let current = navigationController.viewControllers.last {
      setNameVC.modalPresentationStyle = .overCurrentContext
      current.present(setNameVC, animated: true, completion: nil)
    }
  }
  func toAddMusic(delegate: SplashController) {
    let navigator = ImportMusicsNavigator(navigationController: navigationController, dataBaseServices: dataServices, services: services, soundServices: soundServices, suggestion: suggestion)
    let importMusicsVC = ImportMusicsController(navigator: navigator, fileHandler: soundServices.makeAudioFileHandler())
    importMusicsVC.delegate = delegate
    importMusicsVC.loadView()
    importMusicsVC.setupUI()
    if let current = navigationController.viewControllers.last {
      importMusicsVC.modalPresentationStyle = .overCurrentContext
      current.present(importMusicsVC, animated: true, completion: nil)
    }
  }
}

