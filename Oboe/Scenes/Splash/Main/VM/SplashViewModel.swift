//
//  SplashViewModel.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

final class SplashViewModel {
  private let navigator: SplashNavigator
  private let queryManager: QueryManager

  init(navigator: SplashNavigator, queryManager: QueryManager) {
    self.navigator = navigator
    self.queryManager = queryManager
  }
  
  func popSetNameIfNeeded(isFirstTime: Bool, delegate: SplashController) {
    navigator.toSetName(isFirstTime: isFirstTime, delegate: delegate)
  }
  func popAddMusicsIfNeeded(delegate: SplashController) {
    navigator.toAddMusic(delegate: delegate)
  }
  func showPageMenu() {
    navigator.toPageMenu()
  }
  func musicsCount(completion: @escaping (Int)->()) {
    completion(queryManager.getIOManager().getMusicCount())
  }
}
