//
//  SetNameController.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-14.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit
import Domain
import SkyFloatingLabelTextField
import RxSwift

class SetNameController: UIViewController {
  
  // MARK:- Outlets
  @IBOutlet weak var logoLabel: UILabel!
  @IBOutlet weak var logoImageView: UIImageView!
  
  @IBOutlet weak var descriptionTextLabel: UILabel!
  @IBOutlet weak var whatIsYourNameLabel: UILabel!
  @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
  @IBOutlet weak var changeLaterLabel: UILabel!
  
  @IBOutlet weak var saveButton: UIButton!
  @IBOutlet weak var cancelButton: UIButton!
  
  // MARK:- Constants
  private let navigator: SetNameNavigator
  private let queryManager: QueryManager
  private let disposeBag = DisposeBag()
  
  // MARK:- Variables
  var isFirstTime = true
  var delegate: SplashControllerDelegate?
  
  // MARK:- Initialization
  init(navigator: SetNameNavigator, queryManager: QueryManager) {
    self.navigator = navigator
    self.queryManager = queryManager
    super.init(nibName: "SetNameController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK:- LifeCycles
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setNameTexfieldOptions()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    makeAnimations()
  }
  // MARK:- Actions
  @IBAction private func saveButtonPressed(_ sender: UIButton) {
    saveName()
  }
  @IBAction func nameTexfieldDidChange(_ sender: SkyFloatingLabelTextField) {
    if let name = nameTextField.text, name.count > 2 {
      nameTextField.errorMessage = ""
    }
  }
  @IBAction private func cancelButtonPressed(_ sender: UIButton) {
    checkForMusics()
  }
  // MARK:- Fuctions
  private func setNameTexfieldOptions() {
    nameTextField.delegate = self
    if #available(iOS 10.0, *) {
      nameTextField.textContentType = .name
    }
  }
  private func saveName() {
    if let name = nameTextField.text, name.count > 2 {
      UserDefaults.standard.set(name, forKey: Constants.Keys.userFullName.rawValue)
      checkForMusics()
    } else {
      Vibrator.vibrate(hardness: 1)
      nameTextField.errorMessage = Constants.ErrorStrings.enterCorrectName.rawValue
    }
  }
  private func checkForMusics() {
    let musics = queryManager.getIOManager().getMusicCount()
    if musics < 1 {
      delegate?.setNameFinished(haveMusic: false)
    } else {
      delegate?.setNameFinished(haveMusic: true)
    }
    dismiss(animated: true, completion: nil)
  }
}

// MARK:- Extensions
extension SetNameController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    view.endEditing(true)
    saveName()
    return true
  }
}
