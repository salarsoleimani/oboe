//
//  SetNameNavigator.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-21.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Foundation
import Domain

class SetNameNavigator {
  private let navigationController: UINavigationController
  private let services: NetworkUseCaseProvider
  private let soundServices: SoundUsecaseProvider
  private let dataServices: DataBaseUsecaseProvider
  private let suggestion: SuggestionUsecase
  
  init(navigationController: UINavigationController, dataBaseServices: DataBaseUsecaseProvider, services: NetworkUseCaseProvider, soundServices: SoundUsecaseProvider, suggestion: SuggestionUsecase) {
    self.navigationController = navigationController
    self.services = services
    self.soundServices = soundServices
    self.dataServices = dataBaseServices
    self.suggestion = suggestion
  }
  func toAddMusic(delegate: SplashController) {
    let navigator = ImportMusicsNavigator(navigationController: navigationController, dataBaseServices: dataServices, services: services, soundServices: soundServices, suggestion: suggestion)
    let importMusicsVC = ImportMusicsController(navigator: navigator, fileHandler: soundServices.makeAudioFileHandler())
    importMusicsVC.delegate = delegate
    importMusicsVC.loadView()
    importMusicsVC.setupUI()
    if let current = navigationController.viewControllers.last {
      importMusicsVC.modalPresentationStyle = .overCurrentContext
      current.present(importMusicsVC, animated: true, completion: nil)
    }
  }
}
