//
//  SetNameUIUXFunctions.swift
//  Oboe
//
//  Created by Salar Soleimani on 2019-09-27.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import UIKit

extension SetNameController {
  func makeAnimations(_ completion: (() -> Void)? = nil) {
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
      self.descriptionTextLabel.alpha = 1
    })
    UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
      self.whatIsYourNameLabel.alpha = 1
    })
    UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
      self.nameTextField.alpha = 1
      self.changeLaterLabel.alpha = 1
      completion?()
    })
  }
  func setupUI() {
    view.backgroundColor = SColor.darkBlue.value

    logoLabel.text = Constants.Strings.oboe.rawValue
    logoLabel.font = SFont(.custom(SFont.FontFamily.aristotelicaDisplayRegular.rawValue), size: .custom(24)).instance
    
    descriptionTextLabel.text = isFirstTime ? Constants.Strings.nameForFirstTime.rawValue : Constants.Strings.didntEnterNameYet.rawValue
    descriptionTextLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h5)).instance
    descriptionTextLabel.alpha = 0
    
    whatIsYourNameLabel.text = Constants.Strings.whatIsYourName.rawValue
    whatIsYourNameLabel.font = SFont(.installed(StaticConstants.fontFamily, .bold), size: .standard(.h5)).instance
    whatIsYourNameLabel.alpha = 0
    
    nameTextField.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h4)).instance
    nameTextField.placeholder = Constants.Strings.placeHolder.rawValue
    nameTextField.selectedTitle = Constants.Strings.myNameIs.rawValue
    nameTextField.alpha = 0
    
    changeLaterLabel.text = Constants.Strings.changeItLater.rawValue
    changeLaterLabel.font = SFont(.installed(StaticConstants.fontFamily, .regular), size: .standard(.h7)).instance
    changeLaterLabel.alpha = 0
    
    [logoLabel, descriptionTextLabel, whatIsYourNameLabel].forEach{$0?.textColor = SColor.goldYellow.value}
    
    nameTextField.textColor = SColor.goldYellow.value
    nameTextField.selectedLineColor = SColor.goldYellow.value
    nameTextField.lineColor = SColor.lightBlue.value
    nameTextField.titleColor = SColor.goldYellow.value
    
    changeLaterLabel.textColor = SColor.lightBlue.value
    saveButton.setAttributedTitle(Constants.ButtonsText.save.rawValue.toAttributedStringConfirmButton(), for: .normal)
    saveButton.backgroundColor = SColor.lightBlue.value
    cancelButton.setAttributedTitle(Constants.ButtonsText.dontTellMyName.rawValue.toAttributedStringCancelButton(), for: .normal)
  }
}
