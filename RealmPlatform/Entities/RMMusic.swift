//
//  RMMusic.swift
//  RealmPlatform
//
//  Created by Behrad Kazemi on 6/21/19.
//  Copyright © 2019 Behrad Kazemi. All rights reserved.
//

import Domain
import RealmSwift
import Realm

final class RMMusic: Object {
	@objc dynamic var uid = UUID().uuidString
	@objc dynamic var playableID = UUID().uuidString
	@objc dynamic var artistID = UUID().uuidString
	@objc dynamic var albumID = UUID().uuidString
	@objc dynamic var artworkID = UUID().uuidString
	@objc dynamic var title = ""
	@objc dynamic var genre = ""
	@objc dynamic var albumName = ""
	@objc dynamic var artistName = ""
	@objc dynamic var creationDate = Date()
	@objc dynamic var duration = 0.0
	@objc dynamic var playCount = 0
	@objc dynamic var liked = false
	@objc dynamic var rate = 1.0
  @objc dynamic var albumTrackNumber = 1
  @objc dynamic var bpm = 0
  @objc dynamic var skipCount = 0
  @objc dynamic var lastPlayedDate = Date()
  @objc dynamic var lyrics = ""
  @objc dynamic var releasedDate = Date()
	override static func primaryKey() -> String {
		return "uid"
	}
}
extension RMMusic: DomainConvertibleType {
	func asDomain() -> Music {
    return Music(uid: uid, title: title, genre: genre, artworkID: artworkID, artistID: artistID, artistName: artistName, playableID: playableID, creationDate: creationDate, playCount: playCount, albumID: albumID, albumName: albumName, rate: rate, liked: liked, duration: duration, albumTrackNumber: albumTrackNumber, bpm: bpm, skipCount: skipCount, lastPlayedDate: lastPlayedDate, lyrics: lyrics, releasedDate: releasedDate)
	}
}

extension Music: RealmRepresentable {
	func asRealm() -> RMMusic {
		return RMMusic.build { object in
			object.uid = uid
			object.artworkID = artworkID
			object.artistID = artistID
			object.playCount = playCount
			object.playableID = playableID
			object.albumID = albumID
			object.albumName = albumName
			object.artistName = artistName
			object.title = title
			object.genre = genre
			object.duration = duration
			object.rate = rate
			object.liked = liked
			object.creationDate = creationDate
      object.bpm = bpm
      object.albumTrackNumber = albumTrackNumber
      object.skipCount = skipCount
      object.lastPlayedDate = lastPlayedDate
      object.lyrics = lyrics
		}
	}
}
