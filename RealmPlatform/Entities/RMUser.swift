//
//  RMUser.swift
//  RealmPlatform
//
//  Created by Salar Soleimani on 2019-09-23.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

import Domain
import RealmSwift
import Realm

final class RMUser: Object {
  @objc dynamic var uid = UUID().uuidString
  @objc dynamic var name = ""
  @objc dynamic var source = DataSourceType.local.rawValue
  override static func primaryKey() -> String {
    return "uid"
  }
}
