import Foundation
import Realm
import RealmSwift
import RxSwift
import Domain

extension SortDescriptor {
	init(sortDescriptor: NSSortDescriptor) {
		let key = sortDescriptor.key ?? ""
		let asc = sortDescriptor.ascending
		self.init(keyPath: key, ascending: asc)
	}
}
extension Object {
  static func build<O: Object>(_ builder: (O) -> () ) -> O {
    let object = O()
    builder(object)
    return object
  }
}
extension Reactive where Base: Realm {
	func save<R: RealmRepresentable>(entity: R, update: Bool = true) -> Observable<Void> where R.RealmType: Object  {
		return Observable.create { observer in
			do {
				try self.base.write {
          self.base.add(entity.asRealm(), update: .all)
				}
				observer.onNext(())
				observer.onCompleted()
			} catch {
				observer.onError(error)
			}
			return Disposables.create()
		}
	}
	
	func delete<R: RealmRepresentable>(entity: R) -> Observable<Void> where R.RealmType: Object {
		return Observable.create { observer in
			do {
				guard let object = self.base.object(ofType: R.RealmType.self, forPrimaryKey: entity.uid) else { fatalError() }
				
				try self.base.write {
					self.base.delete(object)
				}
				
				observer.onNext(())
				observer.onCompleted()
			} catch {
				observer.onError(error)
			}
			return Disposables.create()
		}
	}
}
