//
//  SoundPlayer.swift
//  SoundsPlatform
//
//  Created by Behrad Kazemi on 6/14/19.
//  Copyright © 2019 Behrad Kazemi. All rights reserved.
//
import UIKit
import AVFoundation
import MediaPlayer
import Domain
import RxSwift

public class SoundPlayer: NSObject, AVAudioPlayerDelegate {
  // MARK: - Properties
  private var audioPlayer = AVAudioPlayer()
  private let audioSession = AVAudioSession()
  var repeatType = MPRepeatType.off {
    didSet {
      switch repeatType {
      case .off:
        numberOfLoops = 0
      case .one:
        numberOfLoops = -1
      case .all:
        numberOfLoops = 1
      @unknown default:
        fatalError()
      }
    }
  }
  
  /// "numberOfLoops" is the number of times that the sound will return to the beginning upon reaching the end.
  /// A value of zero means to play the sound just once.
  /// A value of one will result in playing the sound twice, and so on..
  /// Any negative number will loop indefinitely until stopped.
  public var numberOfLoops: Int = 0 {
    didSet {
      audioPlayer.numberOfLoops = numberOfLoops
    }
  }
  
  /// The volume for the sound. The nominal range is from 0.0 to 1.0.
  public var volume: Float = 1.0 {
    didSet {
      volume = min(1.0, max(0.0, volume))
      targetVolume = volume
    }
  }
  
  /// set panning. -1.0 is left, 0.0 is center, 1.0 is right.
  public var pan: Float = 0.0 {
    didSet {
      audioPlayer.pan = pan
    }
  }
  fileprivate var startVolume: Float = 1.0
  fileprivate var targetVolume: Float = 1.0 {
    didSet {
      audioPlayer.volume = targetVolume
    }
  }
  
  fileprivate var fadeTime: TimeInterval = 0.0
  fileprivate var fadeStart: TimeInterval = 0.0
  fileprivate var timer: Timer?
  
  var shuffled: Bool! {
    didSet {
      if shuffled {
        playingAudios.shuffle()
        return
      }
      playingAudios = audios
    }
  }
  private(set) var current: Playable? {
    didSet {
      currentObs.onNext(current)
    }
  }
  
  private(set) var status: PlayerStatus! {
    didSet {
      statusObs.onNext(status)
    }
  }
  public var currentTime: TimeInterval {
    get {
      return current != nil ? audioPlayer.currentTime : 0
    }
    set {
      audioPlayer.currentTime = newValue
    }
  }
  private(set) var currentObs: BehaviorSubject<Playable?>!
  private(set) var statusObs: BehaviorSubject<PlayerStatus>!
  private(set) var playingAudios: [Playable]! //because if you had shuffle mode you can able to do the action of 'previous' or you can see the exact upnext songs then you turning off the suffle the exact previous order of audios will restore
  private(set) var audios: [Playable]! {
    didSet {
      playingAudios = audios
    }
  }
  //TODO [remove shared]
  public static let shared: SoundPlayer = {
    let manager = SoundPlayer()
    manager.statusObs = BehaviorSubject<PlayerStatus>(value: .stopped)
    manager.currentObs = BehaviorSubject<Playable?>(value: nil)
    manager.status = .stopped
    manager.current = nil
    manager.shuffled = false
    manager.audios = [Playable]()
    do {
      try manager.audioSession.setCategory(.playback)
      try manager.audioSession.setActive(true)
    } catch let error {
      print("error occured when soundplayer is init with err: \(error)")
    }
    RemoteControllersHandler.setupRemoteHandlers(ForManager: manager)
    return manager
  }()
  
  // MARK: - Functions
  func seekTo(desiredTime time: TimeInterval) {
    if audioPlayer.duration >= time && time >= 0 {
      audioPlayer.currentTime = time
    }
  }
  
  public func setup(list: [Playable], index: Int){
    audios = list
    let safeIndex = (index >= list.count) ? (list.count - 1) : index
    current = list[safeIndex]
    play(Model: current!)
  }
  private func play(Model audio: Playable){
    do {
      
      audioPlayer = try AVAudioPlayer(contentsOf: audio.url)
      //				self.audioPlayer.delegate = self
      status = .playing
      current = audio
      audioPlayer.play()
      //				self.updateControlCenter()
    } catch let err {
      next()
      print("Sound Player error with: \(err)")
    }
  }
  
  func pause(){
    status = .paused
    audioPlayer.pause()
  }
  func resume(){
    status = .playing
    audioPlayer.play()
  }
  func stop(){}
  func next(){}
  func previous(){}
  deinit {
      timer?.invalidate()
      audioPlayer.delegate = nil
  }
}
extension SoundPlayer {

    public func fadeTo(volume: Float, duration: TimeInterval = 1.0) {
      startVolume = audioPlayer.volume 
        targetVolume = volume
        fadeTime = duration
        fadeStart = NSDate().timeIntervalSinceReferenceDate
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 0.015, target: self, selector: #selector(handleFadeTo), userInfo: nil, repeats: true)
        }
    }

    public func fadeIn(duration: TimeInterval = 1.0) {
        volume = 0.0
        fadeTo(volume: 1.0, duration: duration)
    }

    public func fadeOut(duration: TimeInterval = 1.0) {
        fadeTo(volume: 0.0, duration: duration)
    }

    @objc func handleFadeTo() {
        let now = NSDate().timeIntervalSinceReferenceDate
        let delta: Float = (Float(now - fadeStart) / Float(fadeTime) * (targetVolume - startVolume))
        let volume = startVolume + delta
        audioPlayer.volume = volume
        if delta > 0.0 && volume >= targetVolume ||
            delta < 0.0 && volume <= targetVolume || delta == 0.0 {
                audioPlayer.volume = targetVolume
                timer?.invalidate()
                timer = nil
                if audioPlayer.volume == 0 {
                    stop()
                }
        }
    }

}
