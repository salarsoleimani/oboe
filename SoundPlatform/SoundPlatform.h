//
//  SoundPlatform.h
//  SoundPlatform
//
//  Created by Salar Soleimani on 2019-09-08.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SoundPlatform.
FOUNDATION_EXPORT double SoundPlatformVersionNumber;

//! Project version string for SoundPlatform.
FOUNDATION_EXPORT const unsigned char SoundPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SoundPlatform/PublicHeader.h>


