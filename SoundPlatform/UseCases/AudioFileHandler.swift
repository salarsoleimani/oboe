//
//  AudioFileHandler.swift
//  SoundsPlatform
//
//  Created by Behrad Kazemi on 7/11/19.
//  Copyright © 2019 Behrad Kazemi. All rights reserved.
//

import Foundation
import RxSwift
import Domain
import MobileCoreServices
import AVFoundation
import GCDWebServer
import MediaPlayer

public final class AudioFileHandler: NSObject, Domain.AudioFileHandler {
  private var webserver: GCDWebUploader?
  public func handleWifiUploader(isStart: Bool, completion: ((String) -> Void)?) {
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first ?? ""
    
    webserver = GCDWebUploader(uploadDirectory: documentsPath)// + "/Musics")
    let port = UInt(54142)
    webserver?.delegate = self
    if let webServer = webserver, isStart, webServer.start(withPort: port, bonjourName: "Web Based Uploads") {
      completion?(webServer.serverURL?.absoluteString ?? "Please turn off and on again.")
    } else {
      webserver?.stop()
      completion?("")
    }
  }
  
  private let manager: Domain.QueryManager
  let disposeBag = DisposeBag()
  
  init(manager: Domain.QueryManager) {
    self.manager = manager
  }
  public func handleAppleMusics(_ music: MPMediaItem, url: URL) -> Observable<Void> {
    // Music properties
    let genre = music.genre ?? Constants.DefaultNames.genre.rawValue
    let lyrics = music.lyrics ?? ""
    let artist = music.artist
    let album = music.albumTitle ?? Constants.DefaultNames.album.rawValue
    let title = music.title ?? Constants.DefaultNames.title.rawValue

    var dateAdded = Date()
    if #available(iOS 10.0, *) {
      dateAdded = music.dateAdded
    }
    let releasedDate = music.releaseDate ?? dateAdded
    let lastPlayedDate = music.lastPlayedDate ?? dateAdded

    let duration = music.playbackDuration
    let playCount = music.playCount
    let skipCount = music.skipCount
    let bpm = music.beatsPerMinute
    let rate = music.rating
    let artworkData = music.artwork?.image(at: CGSize(width: 1024, height: 1024))?.pngData()
    let albumTrackNo = music.albumTrackNumber
    
    // File
    let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    let fileTypeString = String(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,url.absoluteString as CFString,nil)?.takeUnretainedValue() ?? Constants.DefaultNames.fileType.rawValue as CFString)
    let artworkPath = documentsPath.path + "/Artworks/\(artist ?? Constants.DefaultNames.artist.rawValue)_\(title)_\(dateAdded.timeIntervalSinceReferenceDate).jpg".replacingOccurrences(of: ".0", with: "")
    var artworkSavingUrl = URL(fileURLWithPath: artworkPath)
    
    return manager.getSearchingQueries().getPlayable(ofURL: url)
      .filter{ $0.isEmpty }
      .flatMapLatest {[manager] (items) -> Observable<Void> in
      print("There was no item in importing from apple music with: \(url)")
      // Save the artwork to app artwork folder
      do {
        try artworkData?.write(to: artworkSavingUrl)
      } catch let err {
        print("error on saving artwork because : \(err)")
        artworkSavingUrl = Bundle.main.url(forResource: "0", withExtension: "jpg")!
      }
      let artwork = Artwork(uid: UUID().uuidString, dataURL: artworkSavingUrl.absoluteString, source: DataSourceType.local)
      let playable = Playable(uid: UUID().uuidString, url: url, format: fileTypeString, source: .appleMusic)
      if let artistName = artist {
        // if we found an artist there must be at least one album and one music!
        let artistResult = manager.getSingleTableQueries()
          .getArtistQueries()
          .artists(with: artistName)
          .share(replay: 1, scope: .forever)
        let sameArtistResult = artistResult
          .filter{ $0.count > 0 }
          .map{ $0.first! }
        let albumResult = sameArtistResult.flatMapLatest { [manager](artist) -> Observable<[Album]> in
          return manager.getSingleTableQueries()
            .getAlbumsQueries()
            .albums(withName: album, artistID: artist.uid)
            .share(replay: 1, scope: .forever)
        }
        let sameAlbumResult = albumResult
          .filter{$0.count > 0}
          .map{$0.first!}
        
        let audioInsertForArtistResult = Observable.combineLatest(sameArtistResult, sameAlbumResult).flatMapLatest { [manager](artist, album) -> Observable<Music> in
          print("same artist and album \(artist.name)")
          let music = Music(uid: UUID().uuidString, title: title, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: dateAdded, playCount: playCount, albumID: album.uid, albumName: album.title, rate: Double(rate), liked: false, duration: duration, albumTrackNumber: albumTrackNo, bpm: bpm, skipCount: skipCount, lastPlayedDate: lastPlayedDate, lyrics: lyrics, releasedDate: releasedDate)
          let result = Observable.just(music)
          return manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork).withLatestFrom(result)
        }
        let audioInsertForArtistButNewAlbum = Observable.zip(sameArtistResult.share(replay: 1, scope: .forever).take(1), albumResult.filter{$0.count == 0}.mapToVoid().share(replay: 1, scope: .forever).take(1)).flatMapLatest({ [manager](artist, _) -> Observable<Music> in
          print("same artist New Album \(artist.name)")
          let album = Album(uid: UUID().uuidString, artistID: artist.uid, title: album, creationDate: Date(), artworkID: artwork.uid, liked: false)
          let music = Music(uid: UUID().uuidString, title: title, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: dateAdded, playCount: playCount, albumID: album.uid, albumName: album.title, rate: Double(rate), liked: false, duration: duration, albumTrackNumber: albumTrackNo, bpm: bpm, skipCount: skipCount, lastPlayedDate: lastPlayedDate, lyrics: lyrics, releasedDate: releasedDate)
          let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork).share().take(1)
          let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
          let result = Observable.just(music)
          return Observable.zip(insertMusic, insertAlbum).withLatestFrom(result).share().take(1)
        })
        
        let audioInsertForNoArtistResult = artistResult
          .share(replay: 1, scope: .forever)
          .take(1).filter{$0.count == 0}
          .flatMapLatest { [manager]_ -> Observable<Music> in
          print("NEW! artist \(artistName)")
          let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "artist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
          let artist = Artist(uid: UUID().uuidString, name: artistName, artworkID: artistArtwork.uid, liked: false)
          let album = Album(uid: UUID().uuidString, artistID: artist.uid, title: album, creationDate: Date(), artworkID: artwork.uid, liked: false)
          let music = Music(uid: UUID().uuidString, title: title, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: dateAdded, playCount: playCount, albumID: album.uid, albumName: album.title, rate: Double(rate), liked: false, duration: duration, albumTrackNumber: albumTrackNo, bpm: bpm, skipCount: skipCount, lastPlayedDate: lastPlayedDate, lyrics: lyrics, releasedDate: releasedDate)
          let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
          let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
          let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
          let result = Observable.just(music)
          return Observable.zip(insertArtist, insertAlbum, insertMusic).withLatestFrom(result).share(replay: 1, scope: .forever).takeLast(1)
        }
        return Observable.merge(audioInsertForArtistResult, audioInsertForNoArtistResult, audioInsertForArtistButNewAlbum).mapToVoid()
      }
      /* Why not setting artistName a default?
       because if we use default artist name it will always artist result by searching with name so that we need to insert new artist to separate musics that has no artist with eatch other
       */

      let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "artist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
      let artist = Artist(uid: UUID().uuidString, name: Constants.DefaultNames.artist.rawValue, artworkID: artistArtwork.uid, liked: false)
      let album = Album(uid: UUID().uuidString, artistID: artist.artworkID, title: album, creationDate: Date(), artworkID: artwork.uid, liked: false)
      let music = Music(uid: UUID().uuidString, title: title, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: dateAdded, playCount: playCount, albumID: album.uid, albumName: album.title, rate: Double(rate), liked: false, duration: duration, albumTrackNumber: albumTrackNo, bpm: bpm, skipCount: skipCount, lastPlayedDate: lastPlayedDate, lyrics: lyrics, releasedDate: releasedDate)
      let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
      let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
      let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
      return Observable.zip(insertArtist, insertMusic, insertAlbum).share(replay: 1, scope: .forever).takeLast(1).mapToVoid()
    }
  }
  public func handleNewItunesMusic(url: URL, fileType: FileExtensionType) -> Observable<Void> {
    let asset = AVURLAsset(url: url)
    let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    let fileTypeString = String(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,url.absoluteString as CFString,nil)?.takeUnretainedValue() ?? Constants.DefaultNames.fileType.rawValue as CFString)
    let id3Title = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: .common).first
    let id3Artist = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtist, keySpace: .common).first
    let id3Album = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyAlbumName, keySpace: .common).first
    let id3Subject = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeySubject, keySpace: .common).first
    let id3Genre = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.iTunesMetadataKeyUserGenre, keySpace: .iTunes).first
    let id3Artwork = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtwork, keySpace: .common).first
    let id3releasedDate = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.iTunesMetadataKeyReleaseDate, keySpace: .common).first

    let songTitle = id3Title?.value?.copy(with: nil) as? String ?? id3Subject?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.title.rawValue
    let safeAlbumName = id3Album?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.album.rawValue
    
    let artworkData = id3Artwork?.value?.copy(with: nil) as? Data
    let audioDuration = Double(CMTimeGetSeconds(asset.duration))
    let artworkSavingPath = documentsPath.path + "/Artworks/" + String(Date().timeIntervalSinceReferenceDate) + "_" + songTitle
    
    var artworkSavingURL = URL(fileURLWithPath: artworkSavingPath)
    
    return manager.getSearchingQueries().getPlayable(ofURL: url).filter{$0.isEmpty}.flatMapLatest {[manager] (items) -> Observable<Void> in
      print("there was no fucking item with \(url)")
      do {
        try artworkData?.write(to: artworkSavingURL)
      } catch {
        artworkSavingURL = Bundle.main.url(forResource: "0", withExtension: "jpg")!
      }

      let artwork = Artwork(uid: UUID().uuidString, dataURL: artworkSavingURL.absoluteString, source: DataSourceType.local)
      var playable = Playable(uid: UUID().uuidString, url: url, format: fileTypeString)
      playable.source = DataSourceType.iTunes
      let genre = id3Genre?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.genre.rawValue
      if let artistName = id3Artist?.value?.copy(with: nil) as? String {
        // if we found an artist there must be at least one album and one music!
        print("shareing artist name\(artistName)")
        let artistResult = manager.getSingleTableQueries().getArtistQueries().artists(with: artistName).share(replay: 1, scope: .forever)
        print("shareing album name\(safeAlbumName)")
        let sameArtistResult = artistResult.filter{$0.count > 0}.map{$0.first!}
        let sameAlbumResult = sameArtistResult.flatMapLatest { [manager](artist) -> Observable<[Album]> in
          return manager.getSingleTableQueries().getAlbumsQueries().albums(withName: safeAlbumName, artistID: artist.uid).share(replay: 1, scope: .forever)
        }.do(onNext: { (albums) in
          print("albs:\(albums)")
        })
        
        let audioInsertForArtistResult = Observable.combineLatest(sameArtistResult, sameAlbumResult.filter{$0.count > 0}.map{$0.first!}).flatMapLatest { [manager](artist, album) -> Observable<Music> in
          print("same artist and album \(artist.name)")
          let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
          let result = Observable.just(music)
          return manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork).withLatestFrom(result)
        }
        
        let audioInsertForArtistButNewAlbum = Observable.zip(sameArtistResult.share(replay: 1, scope: .forever).take(1), sameAlbumResult.filter{$0.count == 0}.mapToVoid().share(replay: 1, scope: .forever).take(1)).flatMapLatest({ [manager](artist, _) -> Observable<Music> in
          print("same artist New Album \(artist.name)")
          let album = Album(uid: UUID().uuidString, artistID: artist.uid, title: safeAlbumName, creationDate: Date(), artworkID: artwork.uid, liked: false)
          let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
          let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork).share().take(1)
          let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
          let result = Observable.just(music)
          return Observable.zip(insertMusic, insertAlbum).withLatestFrom(result).share().take(1)
          
        })
        
        let audioInsertForNoArtistResult = artistResult.share(replay: 1, scope: .forever).take(1).filter{$0.count == 0}.flatMapLatest { [manager]_ -> Observable<Music> in
          print("NEW! artist \(artistName)")
          let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "artist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
          let artist = Artist(uid: UUID().uuidString, name: artistName, artworkID: artistArtwork.uid, liked: false)
          let album = Album(uid: UUID().uuidString, artistID: artist.uid, title: safeAlbumName, creationDate: Date(), artworkID: artwork.uid, liked: false)
          print("album:\(album)")
          print("artist:\(artist)")
          let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
          let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
          let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
          let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
          let result = Observable.just(music)
          return Observable.zip(insertArtist, insertAlbum, insertMusic).withLatestFrom(result).share(replay: 1, scope: .forever).takeLast(1)
        }
        return Observable.merge(audioInsertForArtistResult,audioInsertForNoArtistResult,audioInsertForArtistButNewAlbum).mapToVoid()
      }
      
      /* Why not setting artistName a default?
       because if we use default artist name it will always artist result by searching with name so that wee need to insert new artist to separate musics that has no artist with eatch other
       */
      print("Hello muda fucker")
      let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "artist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
      let artist = Artist(uid: UUID().uuidString, name: Constants.DefaultNames.artist.rawValue, artworkID: artistArtwork.uid, liked: false)
      let album = Album(uid: UUID().uuidString, artistID: artist.artworkID, title: safeAlbumName, creationDate: Date(), artworkID: artwork.uid, liked: false)
      let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
      let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
      let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
      let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
      return Observable.zip(insertArtist, insertMusic, insertAlbum).share(replay: 1, scope: .forever).takeLast(1).mapToVoid()
    }
  }
  
  public func handleNewMusic(url: URL) -> Observable<Void> {
    let saveFile = savefile(fromURL: url)
    let music = createMusic(fromURL: url).mapToVoid()
    let result = Observable.concat(saveFile, music)
    return result
  }
  
  public func savefile(fromURL url: URL) -> Observable<Void> {
    let audioFileName = url.lastPathComponent
    var safeAudioName = audioFileName.replacingOccurrences(of: " ", with: "_")
    let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    let fileSavingPath = documentsPath.path + "/" + safeAudioName
    if FileManager.default.fileExists(atPath: fileSavingPath){
      safeAudioName = safeAudioName + String(Date().timeIntervalSinceReferenceDate)
    }
    do {
      let audioData = try Data(contentsOf: url)
      let fileSavingURL = URL(fileURLWithPath: fileSavingPath)
      try audioData.write(to: fileSavingURL)
      return Observable.just(())
    } catch let err {
      return Observable.error(err)
    }
  }
  public func createMusic(fromURL url: URL) -> Observable<Music> {
    let asset = AVURLAsset(url: url)
    let documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    let fileTypeString = String(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension,url.absoluteString as CFString,nil)?.takeUnretainedValue() ?? Constants.DefaultNames.fileType.rawValue as CFString)
    let id3Title = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyTitle, keySpace: .common).first
    let id3Artist = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtist, keySpace: .common).first
    let id3Album = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyAlbumName, keySpace: .common).first
    let id3Subject = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeySubject, keySpace: .common).first
    let id3Genre = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyType, keySpace: .common).first
    let id3Artwork = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.commonKeyArtwork, keySpace: .common).first
    let id3releasedDate = AVMetadataItem.metadataItems(from: asset.commonMetadata, withKey: AVMetadataKey.iTunesMetadataKeyReleaseDate, keySpace: .common).first
    let songTitle = id3Title?.value?.copy(with: nil) as? String ?? id3Subject?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.title.rawValue
    let safeAlbumName = id3Album?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.album.rawValue
    let artworkData = id3Artwork?.value?.copy(with: nil) as? Data
    let audioDuration = Double(CMTimeGetSeconds(asset.duration))
    let artworkSavingPath = documentsPath.path + "/Artworks/" + "Artwork_" + songTitle + String(Date().timeIntervalSinceReferenceDate)
    
    var artworkSavingURL = URL(fileURLWithPath: artworkSavingPath)
    do {
      try artworkData?.write(to: artworkSavingURL)
    } catch {
      artworkSavingURL = Bundle.main.url(forResource: "0", withExtension: "jpg")!
    }
    
    let artwork = Artwork(uid: UUID().uuidString, dataURL: artworkSavingURL.absoluteString, source: DataSourceType.local)
    let playable = Playable(uid: UUID().uuidString, url: url, format: fileTypeString)
    
    let genre = id3Genre?.value?.copy(with: nil) as? String ?? Constants.DefaultNames.genre.rawValue
    if let artistName = id3Artist?.value?.copy(with: nil) as? String {
      // if we found an artist there must be at least one album and one music!
      let artistResult = manager.getSingleTableQueries().getArtistQueries().artists(with: artistName)
      let sameArtistResult = artistResult.filter{$0.first != nil}.map{$0.first!}
      let sameAlbumResult = sameArtistResult.flatMapLatest { [manager](artist) -> Observable<Album> in
        return manager.getSingleTableQueries().getAlbumsQueries().albums(withName: safeAlbumName, artistID: artist.uid).filter{$0.first != nil}.map{$0.first!}
      }
      
      let audioInsertForArtistResult = Observable.zip(sameArtistResult, sameAlbumResult).share(replay: 1, scope: .forever).takeLast(1).flatMapLatest { [manager](artist, album) -> Observable<Music> in
        print("same artist \(artist.name)")
        let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
        let result = Observable.just(music)
        return manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork).withLatestFrom(result)
      }
      
      let audioInsertForNoArtistResult = artistResult.filter{$0.first == nil}.flatMapLatest { [manager]_ -> Observable<Music> in
        print("NEW! artist \(artistName)")
        let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "plartist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
        let artist = Artist(uid: UUID().uuidString, name: artistName, artworkID: artistArtwork.uid, liked: false)
        let album = Album(uid: UUID().uuidString, artistID: artist.artworkID, title: safeAlbumName, creationDate: Date(), artworkID: artwork.uid, liked: false)
        
        let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
        let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
        let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
        let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
        let result = Observable.just(music)
        return Observable.zip(insertAlbum, insertMusic, insertArtist).withLatestFrom(result)
      }
      return Observable.merge(audioInsertForArtistResult,audioInsertForNoArtistResult)
    }
    
    /* Why not setting artistName a default?
     because if we use default artist name it will always artist result by searching with name so that wee need to insert new artist to separate musics that has no artist with eatch other
     */
    
    let artistArtwork = Artwork(uid: UUID().uuidString, dataURL: Bundle.main.url(forResource: "artist_placeholder", withExtension: "jpg")!.absoluteString, source: DataSourceType.local)
    let artist = Artist(uid: UUID().uuidString, name: Constants.DefaultNames.artist.rawValue, artworkID: artistArtwork.uid, liked: false)
    let album = Album(uid: UUID().uuidString, artistID: artist.artworkID, title: safeAlbumName, creationDate: Date(), artworkID: artwork.uid, liked: false)
    let music = Music(uid: UUID().uuidString, title: songTitle, genre: genre, artworkID: artwork.uid, artistID: artist.uid, artistName: artist.name, playableID: playable.uid, creationDate: Date(), playCount: 0, albumID: album.uid, albumName: album.title, rate: 1.0, liked: false, duration: audioDuration, albumTrackNumber: 0, bpm: 0, skipCount: 0, lastPlayedDate: Date(), lyrics: "", releasedDate: id3releasedDate?.value?.copy(with: nil) as? Date ?? Date())
    let insertAlbum = manager.getIOManager().insert(Album: album, Artwork: artwork)
    let insertArtist = manager.getIOManager().insert(Artist: artist, Artwork: artistArtwork)
    let insertMusic = manager.getIOManager().insert(Music: music, PlayableModel: playable, Artwork: artwork)
    let result = Observable.just(music)
    return insertAlbum.concat(insertArtist).concat(insertMusic).withLatestFrom(result)
  }
}
extension AudioFileHandler: GCDWebUploaderDelegate {
  
  public func webUploader(_ uploader: GCDWebUploader, didUploadFileAtPath path: String) {
    if let fileExtension = FileExtensionType(rawValue: path.lowercased()) {
      handleNewItunesMusic(url: URL(fileURLWithPath: path), fileType: fileExtension).subscribe().disposed(by: disposeBag)
    } else {
      uploader.shouldDeleteItem(atPath: path)
    }
  }
  public func webUploader(_ uploader: GCDWebUploader, didDeleteItemAtPath path: String) {
    if !path.hasSuffix(".relam") {
    }
  }
}
