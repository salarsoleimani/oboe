//
//  SuggestionPlatform.h
//  SuggestionPlatform
//
//  Created by Salar Soleimani on 2019-09-09.
//  Copyright © 2019 Salar Soleimani. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SuggestionPlatform.
FOUNDATION_EXPORT double SuggestionPlatformVersionNumber;

//! Project version string for SuggestionPlatform.
FOUNDATION_EXPORT const unsigned char SuggestionPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SuggestionPlatform/PublicHeader.h>


